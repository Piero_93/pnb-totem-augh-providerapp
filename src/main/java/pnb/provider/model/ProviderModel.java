package pnb.provider.model;

import java.util.Collection;
import java.util.HashMap;
import pnb.provider.model.Content;


/**
 * Created by Marco on 03/03/2017.
 */
public class ProviderModel {
    private HashMap<Integer, ContentWithStatus> contents;

    public ProviderModel(HashMap<Integer, ContentWithStatus> contents) {
        this.contents = contents;
    }

    public void add(ContentWithStatus c){
        contents.put(c.getId(), c);
    }

    public void remove(int id){
        contents.remove(id);
    }

    public Collection<ContentWithStatus> getContentsList(){
        return contents.values();
    }

    public void setContentStatus(int id, ContentWithStatus.ContentStatus status){
        this.contents.get(id).setStatus(status);
    }
}
