package pnb.provider.model;

import org.jetbrains.annotations.NotNull;
import pnb.provider.internetcommunication.FTPClient;

import java.io.IOException;
import java.util.Calendar;
import java.util.Set;
import java.util.zip.ZipFile;

/**
 * Created by Marco on 05/03/2017.
 */
public class ContentWithStatus extends Content{

    private ContentStatus status;

    public ContentWithStatus(int id, @NotNull String providerId, @NotNull Set<String> tags, @NotNull String url, @NotNull Calendar expirationDate, @NotNull ContentStatus status, int revNumber) {
        super(id, providerId, tags, url, expirationDate, revNumber);
        this.status = status;
    }

    public ContentStatus getStatus() {
        //Prima di ritornare lo status lo controlla
        return status;
    }

    public void setStatus(ContentStatus status) {
        this.status = status;
    }

    public enum ContentStatus{
        VALID, EXPIRED, NOT_REACHABLE, CONTENT_NOT_VALID;
    }

}
