package pnb.provider.model;

import org.jetbrains.annotations.NotNull;
import pnb.provider.utilities.Utilities;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static pnb.provider.utilities.SysKB.*;

/**
 * Created by Biagini on 24/02/2017.
 */
public class ContentBundleImpl implements ContentBundle {
    private String localPath;

    public ContentBundleImpl(int id, @NotNull ZipFile zipFile) throws Exception {
        this.localPath = PATH_PREFIX + id + "/";

        String name = zipFile.getName().split(".zip")[0];

        try {
            //Splash Screen
            InputStream splashScreenInputStream = zipFile.getInputStream(zipFile.getEntry("splashScreen/splashScreen.png"));
            File splashScreenFile = new File(localPath, SPLASH_SCREEN);
            splashScreenFile.getParentFile().mkdirs();
            FileOutputStream splashScreenOutputStream = new FileOutputStream(splashScreenFile);
            int nRead;
            byte[] splashScreenBytes = new byte[1024];
            while ((nRead = splashScreenInputStream.read(splashScreenBytes, 0, splashScreenBytes.length)) != -1) {
                splashScreenOutputStream.write(splashScreenBytes, 0, nRead);
            }
            splashScreenInputStream.close();
            splashScreenOutputStream.close();

            //Thumbnail
            InputStream thumbnailInputStream = zipFile.getInputStream(zipFile.getEntry("thumbnail/thumbnail.png"));
            File thumbnailFile = new File(localPath + THUMBNAIL);
            thumbnailFile.getParentFile().mkdirs();
            FileOutputStream thumbnailOutputStream = new FileOutputStream(localPath + THUMBNAIL);
            byte[] thumbnailBytes = new byte[1024];
            while ((nRead = thumbnailInputStream.read(thumbnailBytes, 0, thumbnailBytes.length)) != -1) {
                thumbnailOutputStream.write(thumbnailBytes, 0, nRead);
            }
            thumbnailInputStream.close();
            thumbnailOutputStream.close();

            //Checking that thumbnail is a square
            BufferedImage thumbnailImage = this.getThumbnail();
            if(thumbnailImage.getWidth() != thumbnailImage.getHeight()) {
                throw new IllegalArgumentException("Zip File content is not a valid content for PNB-Totem");
            }

            //TotemContent && AppContent
            File totemContentFile = new File(localPath + TOTEM_CONTENT);
            totemContentFile.getParentFile().mkdirs();
            File appContentFile = new File(localPath + APP_CONTENT);
            appContentFile.getParentFile().mkdirs();
            FileOutputStream totemContentOutputStream = new FileOutputStream(totemContentFile);
            FileOutputStream appContentOutputStream = new FileOutputStream(appContentFile);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry e = entries.nextElement();
                if (e.getName().startsWith("totemContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), totemContentOutputStream);
                } else if (e.getName().startsWith("appContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), appContentOutputStream);
                }
            }

            totemContentOutputStream.close();
            appContentOutputStream.close();

            zipFile.close();
            Files.delete(Paths.get(zipFile.getName()));

        } catch (NullPointerException | IOException e) {
            throw new IllegalArgumentException("Zip File content is not a valid Content for PNB-Totem");
        }
    }


    @Override
    public BufferedImage getSplashScreen() {
        try {
            return ImageIO.read(new File(localPath + SPLASH_SCREEN));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public BufferedImage getThumbnail() {
        try {
            return ImageIO.read(new File(localPath + THUMBNAIL));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public byte[] getTotemContent() {
        try {
            return Files.readAllBytes(Paths.get(localPath + TOTEM_CONTENT));
        } catch (IOException e) {
            return new byte[0];
        }
    }

    @Override
    public byte[] getAppContent() {
        try {
            return Files.readAllBytes(Paths.get(localPath + APP_CONTENT));
        } catch (IOException e) {
            return new byte[0];
        }
    }

    @Override
    public ZipFile getTotemContentAsZip() {
        try {
            return new ZipFile(localPath + TOTEM_CONTENT);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ZipFile getAppContentAsZip() {
        try {
            return new ZipFile(localPath + APP_CONTENT);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "ContentBundleImpl{" +
                "localPath='" + localPath + '\'' +
                '}';
    }
}
