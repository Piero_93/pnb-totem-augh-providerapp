package pnb.provider.model;

import java.awt.image.BufferedImage;
import java.util.zip.ZipFile;

/**
 * Created by Biagini on 20/02/2017.
 */
public interface ContentBundle {
    BufferedImage getSplashScreen();
    BufferedImage getThumbnail();
    byte[] getTotemContent();
    byte[] getAppContent();
    ZipFile getTotemContentAsZip();
    ZipFile getAppContentAsZip();
}
