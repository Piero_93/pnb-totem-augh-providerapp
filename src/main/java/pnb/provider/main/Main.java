package pnb.provider.main;

import pnb.provider.internetcommunication.ConnectionException;
import pnb.provider.internetcommunication.SwaggerClient;
import pnb.provider.main.starter.RegistrationView;
import io.swagger.client.model.ContentMetaData;
import pnb.provider.model.ContentWithStatus;
import pnb.provider.model.ProviderModel;
import pnb.provider.controller.Controller;
import pnb.provider.view.ProviderView;
import pnb.provider.utilities.Utilities;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Created by Biagini on 15/02/2017.
 */
public class Main {
    public static void main(String[] args) {
        try {
            String username = Utilities.readUsernameFromFile();
            String password = Utilities.readPasswordFromFile();
            if(!SwaggerClient.loginProvider(username, password)){
                new RegistrationView(Main::startSystem);
            } else {
                startSystem();
            }
        } catch (IOException e) {
            new RegistrationView(Main::startSystem);
        }
    }

    public static void startSystem() {
        try {
            HashMap<Integer, ContentWithStatus> contents = new HashMap<>();
            Collection<Integer> contentIds = SwaggerClient.getContentsForProvider(Utilities.readUsernameFromFile());
            for (int i : contentIds) {
                ContentMetaData c = SwaggerClient.getContentWithId(String.valueOf(i));
                contents.put(Integer.parseInt(c.getId()),
                        new ContentWithStatus(Integer.parseInt(c.getId()),
                                c.getProviderId(),
                                new HashSet<>(c.getTag()),
                                c.getUrl(),
                                c.getExpirationDate().toCalendar(Locale.getDefault()),
                                ContentWithStatus.ContentStatus.VALID,
                                c.getRev()
                        )
                );
            }
            ProviderView view = new ProviderView(contents.values().stream().collect(Collectors.toList()));
            ProviderModel model = new ProviderModel(contents);
            Controller controller = new Controller(model);
            view.setObserver(controller);
            controller.setView(view);
            controller.refreshStatusAndView();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
