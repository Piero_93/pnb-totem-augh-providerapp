package pnb.provider.main.starter;

import io.swagger.client.model.RegistrationErrors;
import pnb.provider.internetcommunication.ConnectionException;
import pnb.provider.internetcommunication.SwaggerClient;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.IOException;
import java.util.Optional;

import static pnb.provider.utilities.SysKB.*;
import static pnb.provider.utilities.Utilities.*;


/**
 * Created by Marco on 21/02/2017.
 */
public class RegistrationView extends JFrame{

    private JTextField usernameField;
    private JTextField passwordField;

    private JButton registrationButton;
    private JButton loginButton;


    private JLabel logLabel = new JLabel("");

    private JPanel textPanel;

    private boolean registering = false;

    private ViewObserver observer;

    public RegistrationView(ViewObserver observer) throws HeadlessException {
        this.setLayout(new BorderLayout());
        this.setTitle("Provider App");
        this.observer = observer;

        registrationButton = new JButton("Registrazione");
        loginButton = new JButton("Login");
        registrationButton.setForeground(Color.black);
        loginButton.setForeground(Color.black);
        registrationButton.setBackground(Color.white);
        loginButton.setBackground(Color.white);
        JButton accessButton = new JButton("Accedi");

        usernameField = new JTextField(12);
        passwordField = new JPasswordField(12);


        logLabel = new JLabel("");
        logLabel.setForeground(Color.RED);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        textPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JPanel southPanel = new JPanel(new FlowLayout());

        buttonPanel.add(registrationButton);
        buttonPanel.add(loginButton);

        registrationButton.addActionListener(e -> {
            registering = true;
            refresh(true);
        });

        loginButton.addActionListener(e -> {
            registering = false;
            refresh(false);
        });

        accessButton.addActionListener(e -> {
            new Thread(() -> {
                tryAccess();
            }).start();
        });

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        textPanel.add(new JLabel(USERNAME_JSON_TEXT), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        textPanel.add(usernameField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        textPanel.add(new JLabel(PASSWORD_JSON_TEXT), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 1;
        textPanel.add(passwordField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        textPanel.add(accessButton, c);

        textPanel.setBorder(new EmptyBorder(10, 20, 10, 20));

        southPanel.add(logLabel);

        this.add(buttonPanel, BorderLayout.NORTH);
        this.add(textPanel, BorderLayout.CENTER);
        this.add(southPanel,BorderLayout.SOUTH);

        textPanel.setVisible(false);
        this.setMinimumSize(new Dimension(400, 400));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setResizable(false);
        this.setVisible(true);
    }

    private void tryAccess() {
        if(registering){
            try {
                Optional<RegistrationErrors> errors = SwaggerClient.registerNewProvider(
                        usernameField.getText(),
                        passwordField.getText());
                if(!errors.isPresent()){
                    try {
                        changeUserPw(usernameField.getText(), passwordField.getText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    if(SwaggerClient.loginProvider(usernameField.getText(),passwordField.getText())){
                        SwingUtilities.invokeLater(() -> {
                            this.dispose();
                            observer.userLogged();
                        });
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    if(errors.get().getDuplicatedUsername()){
                        sb.append("USERNAME DUPLICATO.   ");
                    }
                    if(errors.get().getTooShortUsername()){
                        sb.append("USERNAME TROPPO CORTO.   ");
                    }
                    if(errors.get().getTooShortPassword()){
                        sb.append("PASSWORD TROPPO CORTA.   ");
                    }
                    SwingUtilities.invokeLater(() -> logLabel.setText(sb.toString()));
                }

            } catch (ConnectionException e) {
                SwingUtilities.invokeLater(() -> {
                    logLabel.setText("CONNECTION EXCEPTION: CODICE " + e.getErrorCode());
                });
            }
        } else {

            try {
                if(SwaggerClient.loginProvider(usernameField.getText(),passwordField.getText())){
                    changeUserPw(usernameField.getText(),passwordField.getText());
                    SwingUtilities.invokeLater(() -> {
                        observer.userLogged();
                    });
                } else {
                    SwingUtilities.invokeLater(() -> {
                        logLabel.setText("LOGIN FALLITO");
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refresh(boolean registration){
        if(registration){
            textPanel.setVisible(true);
            this.registrationButton.setBackground(Color.RED);
            this.registrationButton.setForeground(Color.white);
            this.loginButton.setBackground(Color.white);
            this.loginButton.setForeground(Color.black);
        } else {
            textPanel.setVisible(true);
            this.registrationButton.setBackground(Color.white);
            this.loginButton.setBackground(Color.RED);
            this.loginButton.setForeground(Color.white);
            this.registrationButton.setForeground(Color.black);
        }
    }

    public interface ViewObserver{
        void userLogged();
    }
}

