package pnb.provider.view;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilCalendarModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import pnb.provider.internetcommunication.ConnectionException;
import pnb.provider.model.Content;
import pnb.provider.utilities.Utilities;

import static pnb.provider.utilities.SysKB.*;

/**
 * View that allows to see and modify a content's fields
 * Created by Marco on 03/03/2017.
 */
class ContentDetailView extends JFrame {

    ContentDetailView(Content content, ContentDetailViewObserver observer){
        this.setTitle("Dettagli Contenuto");
        this.setLayout(new BorderLayout());

        JPanel detailsPanel = new JPanel(new GridBagLayout());
        JPanel logPanel = new JPanel(new FlowLayout());

        Set<String> tags;

        //Initialize all swing components
        JButton saveButton = new JButton(SAVE_BUTTON_TEXT);
        JButton closeButton = new JButton(CLOSE_BUTTON_TEXT);
        JButton addTagButton = new JButton(ADD_TAG_BUTTON_TEXT);
        JButton removeTagButton = new JButton(REMOVE_TAG_BUTTON_TEXT);

        JTextField urlField = new JTextField(20);

        JLabel logLabel = new JLabel("");
        logLabel.setForeground(Color.RED);

        JComboBox<Object> currentTagsBox = new JComboBox<>();
        JComboBox<Object> allTagsBox = new JComboBox<>();
        try {
            allTagsBox.setModel(new DefaultComboBoxModel<>(Utilities.getAllTagsFromServer().toArray()));
        } catch (ConnectionException e) {
            allTagsBox.setModel(new DefaultComboBoxModel<>());
            logLabel.setText("ERRORE CARICAMENTO TAGS DAL SERVER");
        }

        //DatePicker initialization
        UtilCalendarModel dateModel = new UtilCalendarModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        dateModel.setSelected(true);

        //fill URL, DATE AND TAGS if the content is empty or present
        if(content == null) {
            tags = new HashSet<>();
            currentTagsBox.setModel(new DefaultComboBoxModel<>());
        } else {
            tags = content.getTags();
            currentTagsBox.setModel(new DefaultComboBoxModel<>(tags.toArray()));
            urlField.setText(content.getUrl());
            dateModel.setDate(content.getExpirationDate().get(Calendar.YEAR), content.getExpirationDate().get(Calendar.MONTH),content.getExpirationDate().get(Calendar.DAY_OF_MONTH));
        }

        //Structure the details Panel

        JPanel urlPanel = new JPanel(new BorderLayout());
        urlPanel.add(new JLabel(URL_LABEL_TEXT), BorderLayout.WEST);
        urlPanel.add(urlField, BorderLayout.EAST);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        detailsPanel.add(urlPanel, c);
        urlPanel.setBorder(new EmptyBorder(10, 40, 10, 40));

        JPanel edPanel = new JPanel(new GridLayout(0,2));
        edPanel.add(new JLabel(ED_LABEL_TEXT), BorderLayout.WEST);
        edPanel.add(datePicker, BorderLayout.EAST);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        detailsPanel.add(edPanel, c);
        edPanel.setBorder(new EmptyBorder(10, 40, 10, 40));

        JPanel currentTagsPanel = new JPanel(new GridLayout(0,2));
        currentTagsPanel.add(new JLabel(TAGS_LABEL_TEXT));
        currentTagsPanel.add(currentTagsBox);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        detailsPanel.add(currentTagsPanel, c);
        currentTagsPanel.setBorder(new EmptyBorder(10, 40, 30, 40));

        JPanel allTagsPanel = new JPanel(new GridLayout(2,2));
        allTagsPanel.add(new JLabel(SELECT_TAGS_LABEL_TEXT));
        allTagsPanel.add(allTagsBox);
        allTagsPanel.add(addTagButton);
        allTagsPanel.add(removeTagButton);
        ((GridLayout)allTagsPanel.getLayout()).setHgap(10);
        ((GridLayout)allTagsPanel.getLayout()).setVgap(10);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 3;
        detailsPanel.add(allTagsPanel, c);
        //allTagsPanel.setBorder(new EmptyBorder(30, 40, 30, 40));
        allTagsPanel.setBorder(BorderFactory.createTitledBorder("Aggiungi Tag"));
/*
        JPanel tagsButtonPanel = new JPanel(new FlowLayout());
        tagsButtonPanel.add(addTagButton);
        tagsButtonPanel.add(removeTagButton);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 4;
        detailsPanel.add(tagsButtonPanel, c);
        tagsButtonPanel.setBorder(new EmptyBorder(10, 40, 30, 40));
*/
        JPanel windowButtonPanel = new JPanel(new BorderLayout());
        windowButtonPanel.add(saveButton, BorderLayout.WEST);
        windowButtonPanel.add(closeButton, BorderLayout.EAST);
        c.ipady = 20;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 5;
        detailsPanel.add(windowButtonPanel, c);
        windowButtonPanel.setBorder(new EmptyBorder(30, 40, 10, 40));

        detailsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        addTagButton.addActionListener(e -> {
            if(allTagsBox.getModel().getSelectedItem() != null){
                tags.add(allTagsBox.getModel().getSelectedItem().toString());
            }

            currentTagsBox.setModel(new DefaultComboBoxModel<>(tags.toArray()));
        });

        removeTagButton.addActionListener(e -> {
            if(currentTagsBox.getModel().getSelectedItem() != null){
                tags.remove(allTagsBox.getModel().getSelectedItem().toString());
            }

            currentTagsBox.setModel(new DefaultComboBoxModel<>(tags.toArray()));
        });

        saveButton.addActionListener((ActionEvent e) -> {

            //  INPUT CHECKING
            boolean inputsOk = true;
            StringBuilder sb = new StringBuilder();
            if(urlField.getText().isEmpty()){
                inputsOk = false;
                sb.append("URL MANCANTE.");
            }
            if(datePicker.getModel().getValue() == null){
                inputsOk = false;
                sb.append(" DATA MANCANTE.");
            }
            if(tags.isEmpty()){
                inputsOk = false;
                sb.append("NON CI SONO TAGS.");
            }

            logLabel.setText(sb.toString());

            //CALL OBSERVERS TO SAVE CONTENT THEN DISPOSE
            if(inputsOk){
                if(content == null) {
                    observer.saveNewContent(tags,
                            urlField.getText(),
                            (Calendar) datePicker.getModel().getValue());
                } else {
                    observer.saveContentChanges(content.getId(),
                            tags,
                            urlField.getText(),
                            (Calendar) datePicker.getModel().getValue());
                }
            }
            this.dispose();
        });

        closeButton.addActionListener(e -> this.dispose());

        logPanel.add(logLabel);
        this.add(detailsPanel, BorderLayout.CENTER);
        this.add(logPanel, BorderLayout.SOUTH);

        this.setMinimumSize(new Dimension(400,400));
        this.setResizable(false);
        this.setVisible(true);

    }

    class DateLabelFormatter extends JFormattedTextField.AbstractFormatter {

        private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }

    }

    interface ContentDetailViewObserver{
        void saveNewContent(Set<String> tags, String url, Calendar expirationDate);
        void saveContentChanges(int contentId, Set<String> tags, String url, Calendar expirationDate);
    }
}
