package pnb.provider.view;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;

import pnb.provider.model.Content;
import pnb.provider.model.ContentWithStatus;


/**
 * Created by Marco on 03/03/2017.
 */
public class ProviderView extends JFrame implements ContentDetailView.ContentDetailViewObserver{

    private JTable contentTable;
    private ContentsTableModel tableModel;

    private ViewObserver observer;
    JLabel logLabel;

    public ProviderView(List<ContentWithStatus> initialContents) {
        this.setTitle("Provider App");

        this.setLayout(new BorderLayout());
        JPanel southPanel = new JPanel(new GridBagLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());

        JButton newContentButton = new JButton("Nuovo Contenuto");
        JButton removeButton = new JButton("Rimuovi");
        JButton changeButton = new JButton("Modifica");
        JButton refreshButton = new JButton("Aggiorna");

        logLabel = new JLabel("");
        logLabel.setForeground(Color.RED);

        newContentButton.addActionListener(e -> {
            if(observer != null){
                ContentDetailView cdView = new ContentDetailView(null, this);
                cdView.setVisible(true);
            }
        });

        tableModel = new ContentsTableModel(initialContents);
        contentTable = new JTable(tableModel);
        contentTable.setRowSelectionAllowed(true);
        //contentTable.setFillsViewportHeight(true);
        contentTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        contentTable.setCellSelectionEnabled(false);

        JScrollPane tableScrollPane = new JScrollPane(contentTable);

        changeButton.addActionListener(e -> {
            if(contentTable.getSelectedRow() > -1 && observer != null){
                ContentDetailView cdView = new ContentDetailView(((ContentsTableModel)contentTable.getModel()).getContentAtRow(contentTable.getSelectedRow()), this);
                cdView.setVisible(true);
            }
        });

        removeButton.addActionListener(e -> {
            new Thread(() -> {
                this.observer.removeContent((Integer) contentTable.getValueAt(contentTable.getSelectedRow(),0));
            }).start();
        });

        refreshButton.addActionListener(e -> {
            new Thread(() -> {
                this.observer.refreshNeeded();
            }).start();
        });

        buttonPanel.add(newContentButton);
        buttonPanel.add(removeButton);
        buttonPanel.add(changeButton);
        buttonPanel.add(refreshButton);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        southPanel.add(buttonPanel, c);
        buttonPanel.setBorder(new EmptyBorder(10,10,10,10));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        southPanel.add(logLabel, c);

        this.add(tableScrollPane, BorderLayout.CENTER);
        this.add(southPanel,BorderLayout.SOUTH);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setMinimumSize(new Dimension(700,500));
        this.setVisible(true);
    }

    public void refresh(List<ContentWithStatus> contents){
        SwingUtilities.invokeLater(() -> {
            contents.sort(Comparator.comparingInt(Content::getId));
            this.tableModel.setContents(contents);
        });
    }

    public void printLog(String log){
        SwingUtilities.invokeLater(() -> {
            this.logLabel.setText(log);
        });
    }

    public void setObserver(ViewObserver observer) {
        this.observer = observer;
    }

    @Override
    public void saveNewContent(Set<String> tags, String url, Calendar expirationDate) {
        observer.addContent(tags, url, expirationDate);
    }

    @Override
    public void saveContentChanges(int contentId, Set<String> tags, String url, Calendar expirationDate) {
        observer.updateContent(contentId, tags, url, expirationDate);
    }

    class ContentsTableModel extends AbstractTableModel{

        private String[] columnNames = {"ID",
                "URL",
                "Data di Scadenza",
                "Tags",
                "Revision",
                "Status"
        };
        private List<ContentWithStatus> contents;

        public ContentsTableModel(List<ContentWithStatus> contents) {
            this.contents = contents;
        }

        public void setContents(List<ContentWithStatus> contents) {
            this.contents = contents;
            fireTableDataChanged();
        }

        @Override
        public int getRowCount() {
            return contents.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            ContentWithStatus content = (ContentWithStatus) contents.toArray()[rowIndex];

            switch(columnIndex){
                case 0:
                    return content.getId();
                case 1:
                    return content.getUrl();
                case 2:
                    return content.getExpirationDate().getTime();
                case 3:
                    return content.getTags();
                case 4:
                    return content.getRevNumber();
                case 5:
                    switch (content.getStatus()){
                        case VALID:
                            return "VALIDO";
                        case EXPIRED:
                            return "SCADUTO";
                        case CONTENT_NOT_VALID:
                            return "CONTENUTO NON VALIDO";
                        case NOT_REACHABLE:
                            return "NON RAGGIUNGIBILE";
                    }
                default:
                    return null;
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        public ContentWithStatus getContentAtRow(int row){
            if(row < contents.size()){
                return contents.get(row);
            } else {
                throw new IllegalArgumentException("row index not valid: " + row);
            }
        }
    }


    public interface ViewObserver{
        void addContent(Set<String> tags, String url, Calendar expirationDate);

        void removeContent(int id);

        void updateContent(int contentId, Set<String> tags, String url, Calendar expirationDate);

        void refreshNeeded();
    }
}
