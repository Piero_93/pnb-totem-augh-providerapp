package pnb.provider.internetcommunication;

/**
 * Created by Biagini on 15/02/2017.
 */
public class ConnectionException extends Exception {
    private int errorCode;
    private String message;

    public ConnectionException(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
