package pnb.provider.internetcommunication;

import pnb.provider.utilities.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.zip.ZipFile;


/**
 * Created by Biagini on 24/02/2017.
 */
public class FTPClient {

    private FTPClient() {}

    /**
     * Fetch a zipFile from the specified IP Address
     * @param address  the file address
     * @return the read zipFile
     * @throws IOException
     */
    public static ZipFile fetchContent(String address) throws IOException {
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        InputStream inputStream = con.getInputStream();
        Random r = new Random();

        File outputFile;
        do {
            outputFile = new File("temp/" + r.nextInt(Integer.MAX_VALUE) + ".zip");
            outputFile.getParentFile().mkdirs();
        } while (!outputFile.createNewFile());

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        Utilities.copyStream(inputStream, outputStream);
        inputStream.close();
        outputStream.close();

        return new ZipFile(outputFile);
    }
}
