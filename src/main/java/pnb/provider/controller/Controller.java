package pnb.provider.controller;

import io.swagger.client.model.ContentMetaData;
import io.swagger.client.model.ContentRegistrationData;
import javafx.util.Pair;
import org.joda.time.DateTime;
import pnb.provider.internetcommunication.ConnectionException;
import pnb.provider.internetcommunication.FTPClient;
import pnb.provider.internetcommunication.SwaggerClient;
import pnb.provider.model.*;
import pnb.provider.utilities.Utilities;
import pnb.provider.view.ProviderView;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;


/**
 * Created by Marco on 03/03/2017.
 */
public class Controller implements ProviderView.ViewObserver {

    private ProviderModel model;
    private ProviderView view;
    private ExecutorService controllerExecutor;

    public Controller(ProviderModel model) {
        this.model = model;
        this.controllerExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    public void setView(ProviderView view) {
        this.view = view;
    }

    @Override
    public void addContent(Set<String> tags, String url, Calendar expirationDate) {
        controllerExecutor.submit(() -> {
            try {
                int id = SwaggerClient.setContentForProvider(Utilities.readUsernameFromFile(),
                        tags.stream().collect(Collectors.toList()),
                        new DateTime(expirationDate),
                        url
                );

                ContentMetaData metaData = SwaggerClient.getContentWithId(id+"");

                ContentWithStatus.ContentStatus status = this.discoverStatusForContent(new Content(id,
                        metaData.getProviderId(),
                        tags,
                        url,
                        expirationDate,
                        metaData.getRev()
                ));

                model.add(new ContentWithStatus(id,
                        metaData.getProviderId(),
                        tags,
                        url,
                        expirationDate,
                        status,
                        metaData.getRev()
                ));

                this.refreshStatusAndView();

            } catch (ConnectionException e1) {
                this.view.printLog("CONNECTION EXCEPTION: "+ e1.getErrorCode());

            } catch (IOException e1) {
                this.view.printLog("IO EXCEPTION");
            }

        });
    }

    @Override
    public void removeContent(int id) {
        controllerExecutor.submit(() -> {
            try {
                SwaggerClient.deleteContentWithId(String.valueOf(id));
            } catch (ConnectionException e) {
                view.printLog("CONNECTION EXCEPTION: "+ e.getErrorCode());
            }
            this.model.remove(id);
            this.refreshStatusAndView();
        });
    }

    @Override
    public void updateContent(int contentId, Set<String> tags, String url, Calendar expirationDate) {
        controllerExecutor.submit(() -> {
                try {

                    ContentRegistrationData crd = new ContentRegistrationData();
                    crd.setTag(tags.stream().collect(Collectors.toList()));
                    crd.setUrl(url);
                    crd.setExpirationDate(new DateTime(expirationDate));

                    ContentMetaData metaData = SwaggerClient.getContentWithId(contentId+"");
                    SwaggerClient.modifyContentWithId(String.valueOf(contentId), crd);
                    ContentWithStatus.ContentStatus status = this.discoverStatusForContent(new Content(contentId,
                            metaData.getProviderId(),
                            tags,
                            url,
                            expirationDate,
                            metaData.getRev()
                    ));

                    this.model.add(new ContentWithStatus(contentId,
                            metaData.getProviderId(),
                            tags,
                            url,
                            expirationDate,
                            status,
                            metaData.getRev()
                    ));

                    this.refreshStatusAndView();
                } catch (ConnectionException e) {
                    this.view.printLog("CONNECTION EXCEPTION: "+ e.getErrorCode());
                }
            });
    }

    @Override
    public void refreshNeeded() {
        refreshStatusAndView();
    }

    public void refreshStatusAndView(){
        //REDOWNLOADING CONTENTS
        model.getContentsList().stream().parallel().forEach(content -> {
            ContentMetaData metaData = null;
            try {
                metaData = SwaggerClient.getContentWithId(content.getId()+"");

                ContentWithStatus.ContentStatus status = this.discoverStatusForContent(content);

                model.add(new ContentWithStatus(content.getId(),
                        metaData.getProviderId(),
                        content.getTags(),
                        content.getUrl(),
                        content.getExpirationDate(),
                        status,
                        metaData.getRev()
                ));
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        });

        //Check status of every content PARALLEL
        /*Map<Integer, Future<ContentWithStatus.ContentStatus>> allChanges = new HashMap<>();
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (ContentWithStatus c : model.getContentsList()) {
            allChanges.put(c.getId(), executor.submit(() -> discoverStatusForContent(c)));
        }

        for (int i : allChanges.keySet()) {
            try {
                model.setContentStatus(i, allChanges.get(i).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
*/
        this.view.refresh(model.getContentsList().stream().collect(Collectors.toList()));
    }

    private ContentWithStatus.ContentStatus discoverStatusForContent(Content content){
        if(content.getExpirationDate().getTimeInMillis() < System.currentTimeMillis()){
            return ContentWithStatus.ContentStatus.EXPIRED;
        }
        ZipFile zipFile = null;
        try {
            zipFile = FTPClient.fetchContent(content.getUrl());
        } catch (IOException e) {
            return ContentWithStatus.ContentStatus.NOT_REACHABLE;
        }
        try {
            ContentBundle bundle = new ContentBundleImpl(content.getId(), zipFile);
        } catch (Exception e) {
            return ContentWithStatus.ContentStatus.CONTENT_NOT_VALID;
        }

        return ContentWithStatus.ContentStatus.VALID;

    }
}
