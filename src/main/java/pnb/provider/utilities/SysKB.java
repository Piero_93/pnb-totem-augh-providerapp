package pnb.provider.utilities;

/**
 * Created by Marco on 21/02/2017.
 */
public class SysKB {

    //Json Keys
    public static final String USERNAME_JSON_TEXT = "username";
    public static final String PASSWORD_JSON_TEXT = "password";
    
    public static final String CONFIGURATION_FILE = "credentials.providerconf";

    //File managing
    public static final String PATH_PREFIX = "contents/";
    public static final String SPLASH_SCREEN = "splashScreen.png";
    public static final String THUMBNAIL = "thumbnail.png";
    public static final String TOTEM_CONTENT = "totemContent.zip";
    public static final String APP_CONTENT = "appContent.zip";

    //Miscellaneous
    public static final String URL_LABEL_TEXT = "URL";
    public static final String ED_LABEL_TEXT = "Data di Scadenza";
    public static final String TAGS_LABEL_TEXT = "TAGS";
    public static final String SAVE_BUTTON_TEXT = "Salva";
    public static final String CLOSE_BUTTON_TEXT = "Chiudi";
    public static final String ADD_TAG_BUTTON_TEXT = "Aggiungi Tag";
    public static final String SELECT_TAGS_LABEL_TEXT = "Seleziona altri Tags";
    public static final String REMOVE_TAG_BUTTON_TEXT = "Rimuovi Tag";

}
