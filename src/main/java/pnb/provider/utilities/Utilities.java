package pnb.provider.utilities;

import org.json.JSONException;
import org.json.JSONObject;
import pnb.provider.internetcommunication.ConnectionException;
import pnb.provider.internetcommunication.SwaggerClient;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.Set;

/**
 * Created by Biagini on 25/02/2017.
 */
public class Utilities {

    private static String username;
    private static String password;

    /**
     * Read username from the configuration file
     * @return the username
     * @throws Exception
     */
    public static String readUsernameFromFile() throws IOException {
        if(username != null){
            return username;
        } else {
            loadFullConfiguration();
            return username;
        }
    }

    /**
     * Read password from the configuration file
     * @return the password
     * @throws Exception
     */
    public static String readPasswordFromFile() throws IOException {
        if(password != null){
            return password;
        } else {
            loadFullConfiguration();
            return password;
        }
    }

    /**
     * Copies an input stream on an output stream
     * @param input the source
     * @param output the destination
     * @throws IOException
     */
    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        int nRead;
        byte[] data = new byte[1024];
        while((nRead = input.read(data, 0, data.length)) != -1) {
            output.write(data, 0, nRead);
        }
    }

    /**
     * Updates username and password, then updates the providerconf
     * @param user
     * @param pw
     * @throws IOException
     */
    public static void changeUserPw(String user, String pw) throws IOException {
        username = user;
        password = pw;
        byte[] confContent;
        JSONObject json = new JSONObject();
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            json = new JSONObject(new String(confContent, "UTF-8"));
        } catch (JSONException e) {
            throw new IOException(e);
        } catch(NoSuchFileException e){
            //not a previous file exists
        } finally {
            try {
                json.put(SysKB.USERNAME_JSON_TEXT, user);
                json.put(SysKB.PASSWORD_JSON_TEXT, pw);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try(FileWriter file = new FileWriter(SysKB.CONFIGURATION_FILE)){
                file.write(json.toString());
            }
        }
    }

    public static Set<String> getAllTagsFromServer() throws ConnectionException {
        return SwaggerClient.getAllTags();
    }

    /**
     * Load the whole configuration from file (used to lazy-initialize)
     * @throws IOException
     */
    private static void loadFullConfiguration() throws IOException {
        byte[] confContent;
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            JSONObject json = new JSONObject(new String(confContent, "UTF-8"));
            username = json.getString(SysKB.USERNAME_JSON_TEXT);
            password = json.getString(SysKB.PASSWORD_JSON_TEXT);
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }


}
